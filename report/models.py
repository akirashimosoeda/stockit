from django.db import models
from datetime import date
from django.contrib.auth.models import Group

class Report(models.Model):
    date    = models.DateField(u"fecha", default=date.today)
    balance         = models.IntegerField(u"saldo_actual", default=0)
    last_balance    = models.IntegerField(u"saldo anterior", default=0, editable=False)
    expenses        = models.IntegerField(u"gastos", default=0)
    revenue         = models.IntegerField(u"ingresos", default=0)
    #group           = models.ForeignKey(Group, verbose_name=u'grupo', related_name='reports')
    
    class Meta:
        get_latest_by = 'date'
    
    def save(self, *args, **kwargs):
        if not self.pk:
            try:
                self.balance = self.last_balance = Report.objects.latest().balance
            except Report.DoesNotExist:
                pass
        self.balance = self.last_balance + self.revenue - self.expenses
        super(Report, self).save(*args, **kwargs)
    
    def __unicode__(self):
        return '{}: R={} E={} B={}'.format(self.date, self.revenue, self.expenses, self.balance)

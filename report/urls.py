from django.conf.urls import patterns, url
from report import views

urlpatterns = patterns('',
    url(r'^(?P<year>\d+)/$', views.year_report, name='global_year'),
    url(r'^(?P<year>\d+)/(?P<month>\d+)/$', views.month_report, name='global_month'),
    url(r'^(?P<group>\w+)/(?P<year>\d+)/$', views.annual_report, name='group_year'),
    url(r'^(?P<group>\w+)/(?P<year>\d+)/(?P<month>\d+)/$', views.monthly_report, name='group_month'),
    #url(r'^(?P<group>\w+)/(?P<year>\d+)/$', views.year_report, name='year_report'),
    #url(r'^(?P<group>\w+)/(?P<year>\d+)/(?P<month>\d+)/$', views.month_report, name='month_report'),
    )
from django.shortcuts import render
from django.db.models import Sum 
from report.models import Report
from billing.models import BuyBill, SellBill, BuyOrder, SellOrder, Expense
from registry.models import Product
from datetime import date
from django.contrib.auth.models import Group

def year_report(request, year):
    reports = Report.objects.filter(date__year=year).order_by('date') 
    return render(request, 'report/chart.html', {'groups': Group.objects.all(), 'reports': reports, 'year': year})

def month_report(request, year, month):
    report = Report.objects.filter(date__year=year, date__month=month).last()
    expenses = Expense.objects.filter(date__year=year, date__month=month)
    buy_bills = BuyBill.objects.filter(date__year=year, date__month=month)
    sell_bills = SellBill.objects.filter(date__year=year, date__month=month)  
    sales = sell_bills.values_list('orders__product_id', 'orders__product__name').annotate(qty=Sum('orders__quantity'))
    top_cat = sell_bills.values_list('orders__product__category_id', 'orders__product__category__name').annotate(qty=Sum('orders__quantity'))[:5]
    top = sales.order_by('-qty')[:5]
    context = {'groups': Group.objects.all(), 'report': report, 'top_cat': top_cat, 'sales': top, 'top': top[:5], 'expenses': expenses, 'buy_bills': buy_bills,'sell_bills': sell_bills}
    return render(request, 'report/pie.html', context)

def annual_report(request, year, group): 
    reports = []
    group = Group.objects.get(name=group)
    o_filter = {'bill__date__year': year, 'product__group': group.id}
    e_filter = {'date__year': year}
    groups = Group.objects.count()
    for i in range(1, 13):
        o_filter['bill__date__month'] = e_filter['date__month'] = i
        expenses = Expense.objects.filter(**e_filter).aggregate(amount=Sum('price'))['amount'] 
        purchases = BuyOrder.objects.filter(**o_filter).aggregate(amount=Sum('subtotal'))['amount']
        revenue = SellOrder.objects.filter(**o_filter).aggregate(amount=Sum('subtotal'))['amount']
        revenue = revenue if revenue else 0
        loses = (expenses if expenses else 0) / groups + (purchases if purchases else 0)
        profit = revenue - loses
        d = date(int(year), i, 1)
        reports.append(Report(expenses=loses, revenue=revenue, balance=profit, date=d))

    return render(request, 'report/chart.html', {'groups': Group.objects.all(), 'group': group, 'reports': reports, 'year': year})

def monthly_report(request, year, month, group):
    d = date(int(year), int(month), 1)
    group = Group.objects.get(name=group)
    groups = Group.objects.count()
    b_filter = {'date__year': year, 'date__month': month} #bill filter
    o_filter = {'bill__date__year': year, 'bill__date__month': month} #order filter
    outlay = Expense.objects.filter(**b_filter) #Gastos generales
    if group: b_filter['orders__product__group'] = o_filter['product__group'] = group #Filter by group
    #Get filtered objects
    buy_bills = BuyBill.objects.filter(**b_filter).distinct()
    sell_bills = SellBill.objects.filter(**b_filter).distinct()
    expenses = outlay.aggregate(amount=Sum('price'))['amount'] 
    #Calculate Sums
    purchases = BuyOrder.objects.filter(**o_filter).aggregate(amount=Sum('subtotal'))['amount']
    revenue = SellOrder.objects.filter(**o_filter).aggregate(amount=Sum('subtotal'))['amount']
    revenue = revenue if revenue else 0
    loses = (expenses if expenses else 0) / groups + (purchases if purchases else 0) 
    profit = revenue - loses
    
    sales = sell_bills.values_list('orders__product_id', 'orders__product__name').annotate(qty=Sum('orders__quantity'))
    top_cat = sell_bills.values_list('orders__product__category_id', 'orders__product__category__name').annotate(qty=Sum('orders__quantity'))[:5]
    top = sales.order_by('-qty')[:5]
    context = {'groups': Group.objects.all(), 'report': Report(expenses=loses, revenue=revenue, balance=profit, date=d), 'date': d, 'expenses': outlay,
               'buy_bills': buy_bills, 'sell_bills': sell_bills, 'outlay': outlay, 'sales': sales, 'top': top, 'top_cat': top_cat}
    
    return render(request, 'report/pie.html', context)
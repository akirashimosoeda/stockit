from django.views import generic

# VIEWS
class Index(generic.TemplateView):
    template_name = 'index.html'
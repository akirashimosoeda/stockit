# -*- coding: utf-8 -*-

from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import Group

class Category(models.Model):
    class Meta:
        verbose_name = u'categoría'
        verbose_name_plural = u'categorías'
        
    name = models.CharField(u"nombre", max_length=30)
    description = models.TextField(u"descripción", max_length=140, blank=True)
    slug = models.SlugField(u"slug", max_length=30, blank=True, editable=False)
    
    def save(self, *args, **kwargs):
        self.slug = '%s' % (slugify(self.name))
        super(Category, self).save(*args, **kwargs)
    
    def __unicode__(self):
        return self.name

class Product(models.Model):
    class Meta:
        verbose_name = u'producto'
        verbose_name_plural = u'productos'
        
    category        = models.ForeignKey(Category, verbose_name=u'categoría', related_name='products')
    codename        = models.CharField(u"código", max_length=100, unique=True)
    name            = models.CharField(u"nombre", max_length=100)
    description     = models.TextField(u"descripción", max_length=140, blank=True)
    sale_price      = models.IntegerField(u"precio de venta", default=0)
    purchase_price  = models.IntegerField(u"precio de compra", default=0)
    profit          = models.IntegerField(u"lucro", default=0)
    profit_margin   = models.IntegerField(u"margen de lucro", default=0)
    tax             = models.IntegerField(u"tasa", default=10)
    quantity        = models.IntegerField(u"cantidad", default=0)
    picture         = models.ImageField(u"imagen", upload_to='product_pictures', blank=True)
    group           = models.ForeignKey(Group, verbose_name=u'grupo', related_name='products')
    
    def __unicode__(self):
        return self.name

class Customer(models.Model):
    class Meta:
        verbose_name = u'cliente'
        verbose_name_plural = u'clientes'
        
    first_name  = models.CharField(u"Nombre", max_length=30)
    last_name   = models.CharField(u"Apellido", max_length=30)
    phone       = models.CharField(u"Teléfono", max_length=20)
    address     = models.CharField(u"Dirección", max_length=40)
    id_card     = models.CharField(u"CI/RUC", max_length=10)
    city        = models.CharField(u"Ciudad", max_length=20)
    country     = models.CharField(u"País", max_length=20)
    
    def __unicode__(self):
        return self.first_name + " " + self.last_name

class Supplier(models.Model):
    class Meta:
        verbose_name = u'proveedor'
        verbose_name_plural = u'proveedores'
        
    name        = models.CharField(u"Nombre", max_length=30)
    description = models.TextField(u"Descripción", max_length=140)
    phone       = models.CharField(u"Teléfono", max_length=20)
    address     = models.CharField(u"Dirección", max_length=40)
    id_card     = models.CharField(u"CI/RUC", max_length=10)
    city        = models.CharField(u"Ciudad", max_length=20)
    country     = models.CharField(u"País", max_length=20)

    def __unicode__(self):
        return self.name
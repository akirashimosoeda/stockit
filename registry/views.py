from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect
from django.views import generic
from django.contrib.auth.models import Group
from registry.models import Category, Product, Customer, Supplier
from registry import forms
from django_filters.views import FilterView
"""
PRODUCT
"""

class ProductList(FilterView):
    model = Product
    filterset_class = forms.ProductFilter
    template_name = 'product/list.html'
    
class ProductCreate(generic.edit.CreateView):
    template_name = 'product/product_create.html'
    form_class = forms.ProductCreateForm
    success_url = reverse_lazy('product_list')

class ProductUpdate(generic.edit.UpdateView):
    template_name = 'product/product_update.html'
    model = Product
    form_class = forms.ProductUpdateForm
    success_url = reverse_lazy('product_list')

class ProductDelete(generic.edit.DeleteView):
    template_name = 'product/product_delete.html'
    model = Product
    success_url = reverse_lazy('product_list')

class ProductDetail(generic.DetailView):
    template_name = 'product/product_detail.html'
    model = Product
    context_object_name = 'product'
    
    def get_context_data(self, **kwargs):
        context = super(ProductDetail, self).get_context_data(**kwargs)
        context['buy_orders'] = self.object.buy_orders.filter(bill__isnull=False).order_by('bill__date')
        context['sell_orders'] = self.object.sell_orders.filter(bill__isnull=False).order_by('bill__date')
        context['profit_margin'] = self.object.sale_price * (100 - self.object.tax) / 100 - self.object.purchase_price
        return context

"""
CATEGORY
"""

class CategoryList(generic.ListView):
    template_name = 'category/category_list.html'
    model = Category
    context_object_name = 'categories'

class CategoryCreate(generic.edit.CreateView):
    template_name = 'category/category_create.html'
    form_class = forms.CategoryCreateForm
    success_url = reverse_lazy('category_list')
    
class CategoryUpdate(generic.edit.UpdateView):
    template_name = 'category/category_update.html'
    model = Category
    form_class = forms.CategoryUpdateForm
    success_url = reverse_lazy('category_list')
    
class CategoryDelete(generic.edit.DeleteView):
    template_name = 'category/category_delete.html'
    model = Category
    success_url = reverse_lazy('category_list')

class CategoryDetail(generic.DetailView):
    template_name = 'category/category_detail.html'
    model = Category
    context_object_name = 'category'
    def get_context_data(self, **kwargs):
        context = super(CategoryDetail, self).get_context_data(**kwargs)
        context['products'] = self.object.products.order_by('name')
        return context

"""
CUSTOMER
"""

class CustomerList(generic.ListView):
    template_name = 'customer/customer_list.html'
    model = Customer
    context_object_name = 'customers'

class CustomerCreate(generic.edit.CreateView):
    template_name = 'customer/customer_create.html'
    model = Customer
    form_class = forms.CustomerCreateForm
    success_url = reverse_lazy('customer_list')
    
class CustomerUpdate(generic.edit.UpdateView):
    template_name = 'customer/customer_update.html'
    model = Customer
    form_class = forms.CustomerUpdateForm
    success_url = reverse_lazy('customer_list')
    
class CustomerDelete(generic.edit.DeleteView):
    template_name = 'customer/customer_delete.html'
    model = Customer
    success_url = reverse_lazy('customer_list')

class CustomerDetail(generic.DetailView):
    template_name = 'customer/customer_detail.html'
    model = Customer
    context_object_name = 'customer'
    def get_context_data(self, **kwargs):
        context = super(CustomerDetail, self).get_context_data(**kwargs)
        context['bills'] = self.object.sell_bills.order_by('date')
        return context

"""
SUPPLIER
"""

class SupplierList(generic.ListView):
    template_name = 'supplier/supplier_list.html'
    model = Supplier
    context_object_name = 'suppliers'

class SupplierCreate(generic.edit.CreateView):
    template_name = 'supplier/supplier_create.html'
    model = Supplier
    form_class = forms.SupplierCreateForm
    success_url = reverse_lazy('supplier_list')
    
class SupplierUpdate(generic.edit.UpdateView):
    template_name = 'supplier/supplier_update.html'
    model = Supplier
    form_class = forms.SupplierUpdateForm
    success_url = reverse_lazy('supplier_list')
    
class SupplierDelete(generic.edit.DeleteView):
    template_name = 'supplier/supplier_delete.html'
    model = Supplier
    success_url = reverse_lazy('supplier_list')

class SupplierDetail(generic.DetailView):
    template_name = 'supplier/supplier_detail.html'
    model = Supplier
    context_object_name = 'supplier'
    def get_context_data(self, **kwargs):
        context = super(SupplierDetail, self).get_context_data(**kwargs)
        context['bills'] = self.object.buy_bills.order_by('date')
        return context

"""
GROUP
"""

class GroupList(generic.ListView):
    template_name = 'group/list.html'
    model = Group
    context_object_name = 'groups'
    
class GroupCreate(generic.CreateView):
    template_name = 'group/create.html'
    form_class = forms.GroupForm
    success_url = reverse_lazy('group_list')
    
class GroupUpdate(generic.UpdateView):
    template_name = 'group/update.html'
    model = Group
    form_class = forms.GroupForm
    success_url = reverse_lazy('group_list')
    
class GroupDelete(generic.DeleteView):
    template_name = 'group/delete.html'
    model = Group
    success_url = reverse_lazy('group_list')

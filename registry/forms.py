# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import Group, User
from registry.models import Product, Category, Customer, Supplier
import django_filters
"""
PRODUCT
"""    
class ProductCreateForm(forms.ModelForm):
    class Meta:
        model = Product
        exclude = ['purchase_price', 'profit', 'profit_margin', 'quantity']
    
    def clean_tax(self):
        tax = self.cleaned_data['tax']
        if tax < 1:
            raise forms.ValidationError(u'La tasa mínima debe ser 1.')
        return tax
    
class ProductUpdateForm(forms.ModelForm):
    class Meta:
        model = Product
        exclude = ['purchase_price', 'profit', 'profit_margin', 'quantity']
    
class ProductFilter(django_filters.FilterSet):
    codename = django_filters.CharFilter(label='Código', lookup_type='contains')
    name = django_filters.CharFilter(label='Nombre',lookup_type='contains')
    class Meta:
        model = Product
        fields = ['codename', 'name', 'category']
"""
CATEGORY
"""    
class CategoryCreateForm(forms.ModelForm):
    class Meta:
        model = Category

class CategoryUpdateForm(forms.ModelForm):
    class Meta:
        model = Category
"""
CUSTOMER
"""    
class CustomerCreateForm(forms.ModelForm):
    class Meta:
        model = Customer

class CustomerUpdateForm(forms.ModelForm):
    class Meta:
        model = Customer
"""
SUPPLIER
"""       
class SupplierCreateForm(forms.ModelForm):
    class Meta:
        model = Supplier

class SupplierUpdateForm(forms.ModelForm):
    class Meta:
        model = Supplier
        
"""
GROUP
"""
class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
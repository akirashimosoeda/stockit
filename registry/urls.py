from django.conf.urls import patterns, url
from forms import ProductFilter
from registry import views
from django_filters.views import FilterView
urlpatterns = patterns('',
    # PRODUCT
    url(r'^product/list/$', views.ProductList.as_view(), name='product_list'),
    url(r'^product/create/$', views.ProductCreate.as_view(), name='product_create'),
    url(r'^product/update/(?P<pk>\d+)/$', views.ProductUpdate.as_view(), name='product_update'),
    url(r'^product/delete/(?P<pk>\d+)/$', views.ProductDelete.as_view(), name='product_delete'),
    url(r'^product/detail/(?P<pk>\d+)/$', views.ProductDetail.as_view(), name='product_detail'),
    # CATEGORY
    url(r'^category/list/$', views.CategoryList.as_view(), name='category_list'),
    url(r'^category/create/$', views.CategoryCreate.as_view(), name='category_create'),
    url(r'^category/update/(?P<pk>\d+)/$', views.CategoryUpdate.as_view(), name='category_update'),
    url(r'^category/delete/(?P<pk>\d+)/$', views.CategoryDelete.as_view(), name='category_delete'),
    url(r'^category/detail/(?P<pk>\d+)/$', views.CategoryDetail.as_view(), name='category_detail'),
    # CUSTOMER
    url(r'^customer/list/$', views.CustomerList.as_view(), name='customer_list'),
    url(r'^customer/create/$', views.CustomerCreate.as_view(), name='customer_create'),
    url(r'^customer/update/(?P<pk>\d+)/$', views.CustomerUpdate.as_view(), name='customer_update'),
    url(r'^customer/delete/(?P<pk>\d+)/$', views.CustomerDelete.as_view(), name='customer_delete'),
    url(r'^customer/detail/(?P<pk>\d+)/$', views.CustomerDetail.as_view(), name='customer_detail'),
    # SUPPLIER
    url(r'^supplier/list/$', views.SupplierList.as_view(), name='supplier_list'),
    url(r'^supplier/create/$', views.SupplierCreate.as_view(), name='supplier_create'),
    url(r'^supplier/update/(?P<pk>\d+)/$', views.SupplierUpdate.as_view(), name='supplier_update'),
    url(r'^supplier/delete/(?P<pk>\d+)/$', views.SupplierDelete.as_view(), name='supplier_delete'),
    url(r'^supplier/detail/(?P<pk>\d+)/$', views.SupplierDetail.as_view(), name='supplier_detail'),
    # GROUP
    url(r'^group/list/$', views.GroupList.as_view(), name='group_list'),
    url(r'^group/create/$', views.GroupCreate.as_view(), name='group_create'),
    url(r'^group/update/(?P<pk>\d+)/$', views.GroupUpdate.as_view(), name='group_update'),
    url(r'^group/delete/(?P<pk>\d+)/$', views.GroupDelete.as_view(), name='group_delete'),
)

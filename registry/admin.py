from django.contrib import admin
from registry.models import Product, Category, Customer, Supplier

# Register your models here.
admin.site.register(Product)
admin.site.register(Category)
admin.site.register(Customer)
admin.site.register(Supplier)
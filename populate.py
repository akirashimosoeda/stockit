import os

def populate():
    a = Group.objects.get_or_create(name='Other')[0]
    b = Group.objects.get_or_create(name='Minoru')[0]
    cat = Category.objects.get_or_create(name='Video Games', description='Video Games')[0]
    c = Customer.objects.get_or_create(first_name='Gabe', last_name='Newell', phone='456789', address='Black Mesa', id_card='HL3',
                 city='New Mexico', country='USA')
    s = Supplier.objects.get_or_create(name='Valve', phone='456889', address='Aperture Labs', id_card='TF2', city='New Mexico', country='USA')
    p = Product.objects.get_or_create(category=cat, codename='gtav', name='GTA V', description='Grand Theft Auto V',
                                      sale_price=60, group=b, quantity=100, tax=10)
    p = Product.objects.get_or_create(category=cat, codename='mgs5', name='Metal Gear Solid 5', description='The Phantom Pain',
                                      sale_price=60, group=b, quantity=100, tax = 10)
    p = Product.objects.get_or_create(category=cat, codename='ps4', name='Play Station 4', description='Play Station 4',
                                      sale_price=400, group=a, quantity=100, tax = 10)
# Start execution here!
if __name__ == '__main__':
    print "Starting Stockit population script..."
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'stockit.settings')
    from django.contrib.auth.models import Group
    from registry.models import Customer, Supplier, Category, Product
    populate()
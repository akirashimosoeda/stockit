# -*- coding: utf-8 -*-

from django.db import models
from django.db.models import F
from django.core.validators import MinValueValidator
from registry.models import Supplier, Customer, Product
from report.models import Report
import datetime

"""
BILL
"""

class Bill(models.Model):
    class Meta:
        verbose_name = u'factura'
        verbose_name_plural = u'facturas'
        abstract = True
        
    date = models.DateField(u"fecha", default=datetime.date.today, editable=False)
    stamp = models.CharField(u"factura", max_length=30)
    total = models.IntegerField(u"total", editable=False)
        
class BuyBill(Bill):
    class Meta:
        verbose_name = u'factura de compra'
        verbose_name_plural = u'facturas de compra'
        
    supplier = models.ForeignKey(Supplier, related_name='buy_bills', verbose_name=u'proveedor')
    
    def __unicode__(self):
        return self.stamp
    
    def save(self, *args, **kwargs):
        report = Report.objects.filter(date__year=self.date.year, date__month=self.date.month).last()     
        if not report:
            report = Report()
        
        if self.pk: #Updating BuyBill
            old = BuyBill.objects.get(pk=self.pk)
            report.expenses += self.total - old.total
        else: #Creating expense
            report.expenses += self.total
        report.save()
        super(BuyBill, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        report = Report.objects.filter(date__year=self.date.year, date__month=self.date.month).last()
        if report:            
            report.expenses = F('expenses') - self.total
            report.save()
        super(BuyBill, self).delete(*args, **kwargs)
    
class SellBill(Bill):
    class Meta:
        verbose_name = u'factura de venta'
        verbose_name_plural = u'facturas de venta'
        
    customer = models.ForeignKey(Customer, related_name = 'sell_bills', verbose_name = u'cliente')
    
    def __unicode__(self):
        return self.stamp
    
    def save(self, *args, **kwargs):
        report = Report.objects.filter(date__year=self.date.year, date__month=self.date.month).last()            
        if not report:
            report = Report()
        
        if self.pk: #Updating SellBill
            old = SellBill.objects.get(pk=self.pk)
            report.revenue += self.total - old.total
        else: #Creating expense
            report.revenue += self.total
        report.save()
        super(SellBill, self).save(*args, **kwargs)
        
    def delete(self, *args, **kwargs):
        report = Report.objects.filter(date__year=self.date.year, date__month=self.date.month).last()
        if report:            
            report.revenue = F('revenue') - self.total
            report.save()
        super(SellBill, self).delete(*args, **kwargs)
"""
ORDER
"""
  
class Order(models.Model):        
    unit_price = models.IntegerField(u"precio unitario")
    quantity = models.IntegerField(u"cantidad", default=1)
    subtotal = models.IntegerField(u"sub-total", editable=False)
    
    class Meta:
        verbose_name = u'orden'
        verbose_name_plural = u'ordenes'
        abstract = True
        
class BuyOrder(Order):       
    product = models.ForeignKey(Product, related_name="buy_orders", editable=False)
    bill = models.ForeignKey(BuyBill, related_name="orders", null=True, editable=False)
    
    class Meta:
        verbose_name = u'orden de compra'
        verbose_name_plural = u'ordenes de compra'
        
    def __unicode__(self):
        return '{} {}$x{}'.format(self.product, self.unit_price, self.quantity)
    
class SellOrder(Order):      
    product = models.ForeignKey(Product, related_name="sell_orders", editable=False)
    bill = models.ForeignKey(SellBill, related_name="orders", null=True, editable=False)
    discount = models.IntegerField(u"descuento", default=0)
    
    class Meta:
        verbose_name = u'orden de venta'
        verbose_name_plural = u'ordenes de venta'
    
    def __unicode__(self):
        return '{} {}$x{}'.format(self.product, self.unit_price, self.quantity)
    
"""
EXPENSE
"""

class Expense(models.Model):
    stamp = models.CharField(u"factura", max_length=30)
    date = models.DateField(u'fecha', default=datetime.date.today)
    description = models.CharField(u'descripción', max_length=30)
    price = models.IntegerField(u'precio')
    
    class Meta:
        verbose_name = u'gasto'
        verbose_name_plural = u'gastos'
        ordering = ['-date']
        
    def __unicode__(self):
        return 'Gasto {}: {}'.format(self.date, self.price)
    
    def save(self, *args, **kwargs):
        report = Report.objects.filter(date__year=self.date.year, date__month=self.date.month).last()
        if not report:
            report = Report()
        
        if self.pk: #Updating expense
            old = Expense.objects.get(pk=self.pk)
            report.expenses += self.price - old.price 
        else: #Creating expense
            report.expenses += self.price
        report.save()
        super(Expense, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        report = Report.objects.filter(date__year=self.date.year, date__month=self.date.month).last()
        if report:
            report.expenses = F('expenses') - self.price
            report.save()
        super(Expense, self).delete(*args, **kwargs)

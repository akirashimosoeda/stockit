from django.conf.urls import patterns, url
from billing import views

urlpatterns = patterns('',
    # BUY BILL
    url(r'^buy/list/$', views.BuyBillList.as_view(), name='buy_bill_list'),
    url(r'^buy/create/$', views.BuyBillCreate.as_view(), name='buy_bill_create'),
    url(r'^buy/update/(?P<pk>\d+)/$', views.BuyBillUpdate.as_view(), name='buy_bill_update'),
    url(r'^buy/delete/(?P<pk>\d+)/$', views.BuyBillDelete.as_view(), name='buy_bill_delete'),
    url(r'^buy/detail/(?P<pk>\d+)/$', views.BuyBillDetail.as_view(), name='buy_bill_detail'),
    # BUY ORDER
    url(r'^buy/order/$', views.BuyOrderList.as_view(), name='buy_order_list'),
    url(r'^buy/order/select/$', views.BuyOrderCheckProduct.as_view(), name='buy_order_check_product'),
    url(r'^buy/order/select/create/(?P<pk>\d+)/$', views.BuyOrderCreate.as_view(), name='buy_order_create'),
    url(r'^buy/order/delete/(?P<pk>\d+)/$', views.BuyOrderDelete.as_view(), name='buy_order_delete'),
    url(r'^buy/order/clear/$', views.BuyOrderClear.as_view(), name='buy_order_clear'),
    # SELL BILL
    url(r'^sell/list/$', views.SellBillList.as_view(), name='sell_bill_list'),
    url(r'^sell/create/$', views.SellBillCreate.as_view(), name='sell_bill_create'),
    url(r'^sell/update/(?P<pk>\d+)/$', views.SellBillUpdate.as_view(), name='sell_bill_update'),
    url(r'^sell/delete/(?P<pk>\d+)/$', views.SellBillDelete.as_view(), name='sell_bill_delete'),
    url(r'^sell/detail/(?P<pk>\d+)/$', views.SellBillDetail.as_view(), name='sell_bill_detail'),
    # SELL ORDER
    url(r'^sell/order/$', views.SellOrderList.as_view(), name='sell_order_list'),
    url(r'^sell/order/select/$', views.SellOrderCheckProduct.as_view(), name='sell_order_check_product'),
    url(r'^sell/order/select/create/(?P<pk>\d+)/$', views.SellOrderCreate.as_view(), name='sell_order_create'),
    url(r'^sell/order/delete/(?P<pk>\d+)/$', views.SellOrderDelete.as_view(), name='sell_order_delete'),
    url(r'^sell/order/clear/$', views.SellOrderClear.as_view(), name='sell_order_clear'),
    # EXPENSE
    url(r'^expense/$', views.ExpenseList.as_view(), name='expense_list'),
    url(r'^expense/create/$', views.ExpenseCreate.as_view(), name='expense_create'),
    url(r'^expense/update/(?P<pk>\d+)/$', views.ExpenseUpdate.as_view(), name='expense_update'),
    url(r'^expense/delete/(?P<pk>\d+)/$', views.ExpenseDelete.as_view(), name='expense_delete'),
)

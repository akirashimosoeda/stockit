# -*- coding: utf-8 -*-

from django import forms 
from models import BuyBill, SellBill, BuyOrder, SellOrder, Expense
from registry.models import Product, Supplier, Customer

"""
BUY BILL
"""
class BuyBillCreateForm(forms.Form):
    stamp = forms.CharField(label = u'Factura')
    supplier = forms.ModelChoiceField(queryset = Supplier.objects.all(), label = u'Proveedor', required = False)
    id_card = forms.CharField(label = u'CI/RUC', required = False)
    
    def clean(self):
        cleaned_data = super(BuyBillCreateForm, self).clean()
        id_card = cleaned_data.get('id_card')
        supplier = cleaned_data.get('supplier')
        if not (id_card or supplier):
            raise forms.ValidationError(u'Complete el campo proveedor o CI/RUC')
        return cleaned_data
    
    def clean_id_card(self):
        id_card = self.cleaned_data['id_card']
        if id_card:
            try:
                Supplier.objects.get(id_card=id_card)
            except Supplier.DoesNotExist:
                raise forms.ValidationError(u"El proveedor con CI/RUC '"+str(id_card)+u"' no existe.")
        return id_card

class BuyBillUpdateForm(forms.ModelForm):
    class Meta:
        model = BuyBill

"""
SELL BILL
"""
class SellBillCreateForm(forms.Form):
    stamp = forms.CharField(label = u'Factura')
    customer = forms.ModelChoiceField(queryset = Customer.objects.all(), label = u'Cliente', required = False)
    id_card = forms.CharField(label = u'CI/RUC', required = False)
    
    def clean(self):
        cleaned_data = super(SellBillCreateForm, self).clean()
        id_card = cleaned_data.get('id_card')
        customer = cleaned_data.get('customer')
        if not (id_card or customer):
            raise forms.ValidationError(u'Complete el campo cliente o CI/RUC')
        return cleaned_data
    
    def clean_id_card(self):
        id_card = self.cleaned_data['id_card']
        if id_card:
            try:
                Customer.objects.get(id_card=id_card)
            except Customer.DoesNotExist:
                raise forms.ValidationError(u"El cliente con CI/RUC '"+str(id_card)+u"' no existe.")
        return id_card
    
class SellBillUpdateForm(forms.ModelForm):
    class Meta:
        model = SellBill
"""
ORDER
"""

"""
BUY ORDER
"""

class BuyOrderCheckProductForm(forms.Form):
    codename = forms.CharField(label=u'Código')
    
    def clean_codename(self):
        try:
            product = Product.objects.get(codename=self.cleaned_data['codename'])
            # Prevent duplicate order products
            if product.buy_orders.filter(bill__isnull = True):
                raise forms.ValidationError(u"El producto '"+self.cleaned_data['codename']+u"' ya existe en la orden.")
        except Product.DoesNotExist:
            raise forms.ValidationError(u"El producto con el código '"+self.cleaned_data['codename']+"' no existe.")
        return product.codename
    
class BuyOrderCreateForm(forms.ModelForm):
    class Meta:
        model = BuyOrder
        
    def clean_unit_price(self):
        unit_price = self.cleaned_data['unit_price']
        if unit_price < 1:
            raise forms.ValidationError(u'El precio mínimo debe ser 1.')
        return unit_price
    
    def clean_quantity(self):
        quantity = self.cleaned_data['quantity']
        if quantity < 1:
            raise forms.ValidationError(u'La cantidad mínima debe ser 1.')
        return quantity
    
class BuyOrderUpdateForm(forms.ModelForm):
    class Meta:
        model = BuyOrder

"""
SELL ORDER
"""
class SellOrderCheckProductForm(forms.Form):
    codename = forms.CharField(label=u'Código')
    
    def clean_codename(self):
        try:
            product = Product.objects.get(codename=self.cleaned_data['codename'])
            # Prevent duplicate order products
            if product.sell_orders.filter(bill__isnull = True):
                raise forms.ValidationError(u"El producto '"+self.cleaned_data['codename']+u"' ya existe en la orden.")
            # Prevent grabbing 0 stock products
            if product.quantity == 0:
                raise forms.ValidationError(u"El producto '"+self.cleaned_data['codename']+u"' no tiene stock.")
        except Product.DoesNotExist:
            raise forms.ValidationError(u"El producto con el código '"+self.cleaned_data['codename']+"' no existe.")
        return product.codename
    
class SellOrderCreateForm(forms.ModelForm):  
    class Meta:
        model = SellOrder
        exclude = ['unit_price']
        
    def __init__(self, *args, **kwargs):
        self.product = kwargs.pop('product', None)
        super(SellOrderCreateForm, self).__init__(*args, **kwargs)
        
    def clean_quantity(self):
        if self.product.quantity < int(self.cleaned_data['quantity']):
            raise forms.ValidationError(u"Usted solo tiene "+str(self.product.quantity)+u" en stock.")
        return int(self.cleaned_data['quantity'])
    
class SellOrderUpdateForm(forms.ModelForm):
    class Meta:
        model = SellOrder
"""
EXPENSE
"""

class ExpenseCreateForm(forms.ModelForm):
    class Meta:
        model = Expense
        
class ExpenseUpdateForm(forms.ModelForm):
    class Meta:
 
        model = Expense
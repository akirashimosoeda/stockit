from django.contrib import admin
from billing.models import BuyBill, SellBill, BuyOrder, SellOrder, Expense

admin.site.register(BuyBill)
admin.site.register(SellBill)
admin.site.register(BuyOrder)
admin.site.register(SellOrder)
admin.site.register(Expense)
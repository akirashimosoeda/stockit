from django.shortcuts import redirect, render
from django.core.urlresolvers import reverse_lazy
from django.views import generic
from billing.models import BuyBill, SellBill, BuyOrder, SellOrder, Expense
from billing import forms
from registry.models import Product, Supplier, Customer
import datetime

"""
BUY BILL
"""

class BuyBillList(generic.ListView):
    template_name='buy_bill/list.html'
    model = BuyBill
    context_object_name = 'bills'
    
class BuyBillCreate(generic.FormView):
    template_name = 'buy_bill/create.html'
    form_class = forms.BuyBillCreateForm
    
    def get_context_data(self, **kwargs):
        context = super(BuyBillCreate, self).get_context_data(**kwargs)
        orders = BuyOrder.objects.filter(bill__isnull=True)
        total = 0
        for order in orders:
            total += order.subtotal
        context['orders'] = orders
        context['total'] = total
        return context
        
    def form_valid(self, form):
        orders = BuyOrder.objects.filter(bill__isnull = True)
        total = 0
        for order in orders:
            total += order.subtotal
            order.product.purchase_price = order.unit_price
            order.product.quantity += order.quantity
            order.product.save()
        id_card = form.cleaned_data['id_card']
        print id_card
        if id_card:
            print Supplier.objects.get(id_card=id_card)
            bill = BuyBill.objects.create(stamp = form.cleaned_data['stamp'],
                                          total = total,
                                          supplier = Supplier.objects.get(id_card=id_card))
        else:
            bill = BuyBill.objects.create(stamp = form.cleaned_data['stamp'],
                                          total = total,
                                          supplier = Supplier.objects.get(id = form.cleaned_data['supplier'].id))
        orders.update(bill = bill)
        return redirect('buy_bill_list')

class BuyBillUpdate(generic.UpdateView):
    template_name = 'buy_bill/update.html'
    model = BuyBill
    form_class = forms.BuyBillUpdateForm
    
class BuyBillDelete(generic.DeleteView):
    template_name = 'buy_bill/delete.html'
    model = BuyBill
    success_url = reverse_lazy('buy_bill_list')
    
    def delete(self, request, *args, **kwargs):
        bill = BuyBill.objects.get(pk=self.kwargs['pk'])
        for order in bill.orders.all():
            order.product.quantity -= order.quantity
            order.product.save()
        return super(BuyBillDelete, self).delete(request, *args, **kwargs)
    
class BuyBillDetail(generic.DetailView):
    template_name = 'buy_bill/detail.html'
    model = BuyBill
    context_object_name = 'bill'
    def get_context_data(self, **kwargs):
        context = super(BuyBillDetail, self).get_context_data(**kwargs)
        orders = self.object.orders.all()
        tax = 0
        for order in orders:
            tax += order.subtotal * order.product.tax / 100
        context['orders'] = orders
        context['tax'] = tax
        return context
"""
BUY ORDER
"""

class BuyOrderList(generic.ListView):
    template_name = 'buy_order/list.html'
    model = BuyOrder
    context_object_name = 'buy_orders'
    queryset = BuyOrder.objects.filter(bill__isnull=True)
    
    def get_context_data(self, **kwargs):
        orders = BuyOrder.objects.filter(bill__isnull=True)
        total = 0
        tax = 0
        for order in orders:
            total += order.subtotal
            tax +=  order.subtotal * order.product.tax / 100
        context = super(BuyOrderList, self).get_context_data(**kwargs)
        context['total'] = total
        context['tax'] = tax
        return context
    
class BuyOrderCheckProduct(generic.FormView):
    template_name = 'buy_order/check_product.html'
    form_class = forms.BuyOrderCheckProductForm
    
    def form_valid(self, form):
        product = Product.objects.get(codename=form.cleaned_data['codename'])
        return redirect('buy_order_create', pk=product.pk)
        
class BuyOrderCreate(generic.CreateView):
    template_name = 'buy_order/create.html'
    form_class = forms.BuyOrderCreateForm
    
    def get_context_data(self, **kwargs):
        context = super(BuyOrderCreate, self).get_context_data(**kwargs)
        context['product'] = Product.objects.get(pk=self.kwargs['pk'])
        return context
    
    def form_valid(self, form):
        BuyOrder.objects.create(unit_price = form.cleaned_data['unit_price'],
                                quantity = form.cleaned_data['quantity'],
                                subtotal = form.cleaned_data['unit_price'] * form.cleaned_data['quantity'],
                                product = Product.objects.get(pk=self.kwargs['pk']))
        return redirect('buy_order_list')
    
class BuyOrderDelete(generic.DeleteView):
    template_name = 'buy_order/delete.html'
    model = BuyOrder
    success_url = reverse_lazy('buy_order_list')

class BuyOrderClear(generic.View): 
    template_name = 'buy_order/clear.html'
    
    def get(self, request, *args, **kwargs):
        orders = BuyOrder.objects.filter(bill__isnull = True)
        orders.delete()
        return render(request, 'buy_order/list.html')
    
"""
SELL BILL
"""

class SellBillList(generic.ListView):
    template_name = 'sell_bill/list.html'
    model = SellBill
    context_object_name = 'bills'

class SellBillCreate(generic.FormView):
    template_name = 'sell_bill/create.html'
    form_class = forms.SellBillCreateForm
        
    def get_context_data(self, **kwargs):
        orders = SellOrder.objects.filter(bill__isnull=True)
        subtotal = 0
        for order in orders:
            subtotal += order.unit_price * order.quantity * (100 - order.discount) / 100
        context = super(SellBillCreate, self).get_context_data(**kwargs)
        context['date'] = datetime.date.today
        context['orders'] = orders
        context['subtotal'] = subtotal
        return context
        
    def form_valid(self, form):
        orders = SellOrder.objects.filter(bill__isnull=True)
        total = 0
        for order in orders:
            total += order.subtotal
            order.product.sale_price = order.unit_price
            order.product.quantity -= order.quantity
            order.product.save()
        id_card = form.cleaned_data['id_card']
        if id_card:
            bill = SellBill.objects.create(stamp = form.cleaned_data['stamp'],
                                          total = total,
                                          customer = Customer.objects.get(id_card = id_card))
        else:
            bill = SellBill.objects.create(stamp = form.cleaned_data['stamp'],
                                          total = total,
                                          customer = Customer.objects.get(id=form.cleaned_data['customer'].id))
        orders.update(bill = bill)
        return redirect('sell_bill_list')

class SellBillUpdate(generic.UpdateView):
    template_name = 'sell_bill/update.html'
    model = SellBill
    form_class = forms.SellBillUpdateForm

class SellBillDelete(generic.DeleteView):
    template_name = 'sell_bill/delete.html'
    model = SellBill
    success_url = reverse_lazy('sell_bill_list')

    def delete(self, request, *args, **kwargs):
        bill = SellBill.objects.get(pk=self.kwargs['pk'])
        for order in bill.orders.all():
            order.product.quantity += order.quantity
            order.product.save()
        return super(SellBillDelete, self).delete(request, *args, **kwargs)
    
class SellBillDetail(generic.DetailView):
    template_name = 'sell_bill/detail.html'
    model = SellBill
    context_object_name = 'bill'
    def get_context_data(self, **kwargs):
        context = super(SellBillDetail, self).get_context_data(**kwargs)
        orders = self.object.orders.all()
        tax = 0
        for order in orders:
            tax += order.subtotal * order.product.tax / 100
        context['orders'] = orders
        context['tax'] = tax
        return context

"""
SELL ORDER
"""

class SellOrderList(generic.ListView):
    template_name = 'sell_order/list.html'
    model = SellOrder
    context_object_name = 'sell_orders'
    queryset = SellOrder.objects.filter(bill__isnull=True)
    
    def get_context_data(self, **kwargs):
        orders = SellOrder.objects.filter(bill__isnull=True)
        total = 0
        tax = 0
        for order in orders:
            total += order.subtotal
            tax +=  order.subtotal / order.product.tax
        context = super(SellOrderList, self).get_context_data(**kwargs)
        context['total'] = total
        context['tax'] = tax
        return context
    
class SellOrderCheckProduct(generic.FormView):
    template_name = 'sell_order/check_product.html'
    form_class = forms.SellOrderCheckProductForm
    
    def form_valid(self, form):
        product = Product.objects.get(codename=form.cleaned_data['codename'])
        return redirect('sell_order_create', pk=product.pk)
    
class SellOrderCreate(generic.CreateView):
    template_name = 'sell_order/create.html'
    form_class = forms.SellOrderCreateForm
    
    def get_context_data(self, **kwargs):
        context = super(SellOrderCreate, self).get_context_data(**kwargs)
        context['product'] = Product.objects.get(pk=self.kwargs['pk'])
        return context
    
    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(SellOrderCreate, self).get_form_kwargs(*args, **kwargs)
        kwargs.update({'product':Product.objects.get(pk=self.kwargs['pk'])})
        return kwargs
    
    def form_valid(self, form):
        product = Product.objects.get(pk=self.kwargs['pk'])
        quantity = form.cleaned_data['quantity']
        discount = form.cleaned_data['discount']
        subtotal = product.sale_price * quantity * (100 - discount) / 100
        SellOrder.objects.create(unit_price=product.sale_price,
                                 quantity = quantity,
                                 discount = discount,
                                 subtotal = subtotal,
                                 product = product)
        return redirect('sell_order_list')
    
class SellOrderDelete(generic.DeleteView):
    template_name = 'sell_order/delete.html'
    model = SellOrder
    success_url = reverse_lazy('sell_order_list')
    
class SellOrderClear(generic.View): 
    """
    """
    def get(self, request, *args, **kwargs):
        orders = SellOrder.objects.filter(bill__isnull = True)
        orders.delete()
        return render(request, 'sell_order/list.html')
"""
EXPENSE
"""

class ExpenseList(generic.ListView):
    template_name = 'expense/expense_list.html'
    model = Expense
    context_object_name = 'expenses'
    
    def get_context_data(self, **kwargs):
        context = super(ExpenseList, self).get_context_data(**kwargs)
        total = 0
        for expense in Expense.objects.all():
            total += expense.price
        context['total'] = total
        return context
    
class ExpenseCreate(generic.edit.CreateView):
    template_name = 'expense/expense_create.html'
    model = Expense
    form_class = forms.ExpenseCreateForm
    success_url = reverse_lazy('expense_list')
    
class ExpenseUpdate(generic.edit.UpdateView):
    template_name = 'expense/expense_update.html'
    model = Expense
    form_class = forms.ExpenseUpdateForm
    success_url = reverse_lazy('expense_list')
    
class ExpenseDelete(generic.edit.DeleteView):
    template_name = 'expense/expense_delete.html'
    model = Expense
    success_url = reverse_lazy('expense_list')
